package edu.uprm.cse.datastructures.problems;

import javax.naming.spi.DirStateFactory.Result;

import edu.uprm.cse.datastructures.set.ArraySet;
import edu.uprm.cse.datastructures.set.Set;

public class Disjoint {

	// ADD YOUR CODE HERE

	private static boolean checkDisjoint(Object[] sets) {
		for(int i=0; i<sets.length; i++) {
			ArraySet<Object> currentSet = (ArraySet<Object>) sets[i];
			for(int x=1; x<sets.length; x++) {
				ArraySet<Object> nextSet = (ArraySet<Object>) sets[x];
				if(currentSet.equals(nextSet)) {
					continue;
				}
//				if(currentSet.equals(null) || nextSet.equals(null)) {
//					break;
//				}
				if(!currentSet.intersection(nextSet).isEmpty()) {
					return false;
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		Set<Integer> S1 = new  ArraySet<Integer>();
		Set<Integer> S2 = new  ArraySet<Integer>();
		Set<Integer> S3 = new  ArraySet<Integer>();
		Set<Integer> S4 = new  ArraySet<Integer>();

		S1.add(1);
		S1.add(2);

		S2.add(0);

		S3.add(5);
		S3.add(8);
		S3.add(9);

		S4.add(5);

		@SuppressWarnings("unchecked")
		Object[] A1 =   new Object[3];
		A1[0] = S1;
		A1[1] = S2;
		A1[2] = S3;

		System.out.println("S1, S2, and S3 disjoint: " + checkDisjoint(A1));
		Object[] A2 =   new Object[4];
		A2[0] = S1;
		A2[1] = S2;
		A2[2] = S3;
		A2[3] = S4;

		System.out.println("S1, S2, S3, and S4 disjoint: " + checkDisjoint(A2));
		
		Set<Integer> S5 = new  ArraySet<Integer>();
		Set<Integer> S6 = new  ArraySet<Integer>();
		Set<Integer> S7 = new  ArraySet<Integer>();
		
		S5.add(6);
		S5.add(7);
		S5.add(8);

		S6.add(2);
		S6.add(3);
		S6.add(4);
		
		S7.add(1);
		S7.add(2);
		S7.add(8);

		Object[] A3 =   new Object[2];
		A3[0] = S5;
		A3[1] = S6;

		Object[] A4 =   new Object[3];
		A4[0] = S5;
		A4[1] = S6;
		A4[2] = S7;

		System.out.println("-- Additional Tests! --");
		System.out.println("S5, S6 disjoint: " + checkDisjoint(A3));

		System.out.println("S5, S6, and S7 disjoint: " + checkDisjoint(A4));

	}

}
